#Needs access to Hospital_CA.crt and Hospital_key.key
#$1 is desiered portID

trustStore=../cert_project_2/ServerStuff/$1/servertruststore
keyStore=../cert_project_2/serverStuff/$1/serverkeystore
serverFolder=../cert_project_2/serverStuff/$1/


mkdir -p "../cert_project_2/serverStuff/$1"
keytool -genkey -keyalg RSA -alias $1 -keystore $keyStore
keytool -keystore $keyStore -certreq -alias $1 -keyalg rsa -file $serverFolder$1.csr
openssl x509 -req -CA "../cert_project_2/CA_and_key/Hospital_CA.crt" -CAkey "../cert_project_2/CA_and_key/Hospital_key.key" -in $serverFolder$1.csr -out $serverFolder$1.crt -CAcreateserial
keytool -import -keystore $keyStore -file "../cert_project_2/CA_and_key/Hospital_CA.crt" -alias Hospital_CA
keytool -import -keystore $keyStore -file $serverFolder$1.crt -alias $1
keytool -import -file "../cert_project_2/CA_and_key/Hospital_CA.crt" -alias Hospital_CA -keystore $trustStore
