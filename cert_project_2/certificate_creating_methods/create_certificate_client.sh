# arg 1 = Username

#Needs access to Hospital_CA.crt and Hospital_key.key

trustStore=../cert_project_2/ClientStuff/$1/clienttruststore
keyStore=../cert_project_2/ClientStuff/$1/clientkeystore
clientFoler=../cert_project_2/ClientStuff/$1/


mkdir -p "../cert_project_2/ClientStuff/$1"
keytool -import -file ../cert_project_2/CA_and_key/Hospital_CA.crt -alias Hospital_CA -keystore $trustStore
keytool -genkey -keyalg RSA -alias $1 -keystore $keyStore
keytool -keystore $keyStore -certreq -alias $1 -keyalg rsa -file $clientFoler/$1.csr
openssl x509 -req -CA "../cert_project_2/CA_and_key/Hospital_CA.crt" -CAkey "../cert_project_2/CA_and_key/Hospital_key.key" -in $clientFoler/$1.csr -out $clientFoler/$1.crt -CAcreateserial
keytool -import -keystore $keyStore -file ../cert_project_2/CA_and_key/Hospital_CA.crt -alias Hospital_CA
keytool -import -keystore $keyStore -file $clientFoler/$1.crt -alias $1

