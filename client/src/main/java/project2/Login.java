package project2;

import java.net.*;
import java.io.*;
import javax.net.ssl.*;
import java.security.cert.X509Certificate;
import java.util.stream.Collectors;
import java.security.KeyStore;
import java.security.cert.*;

/*
 * This example shows how to set up a key manager to perform client
 * authentication.
 *
 * This program assumes that the client is not inside a firewall.
 * The application can be modified to connect to a server outside
 * the firewall by following SSLSocketClientWithTunneling.java.
 */
public class Login {

    private static String pathToKeyAndTrustStores= "../cert_project_2/ClientStuff/";
    public static void main(String[] args) throws Exception {

        System.out.println(System.getProperty("user.dir"));
        char[] clientKeyStorePasswd = args[2].toCharArray();
        char[] clientTrustStorePasswd = args[3].toCharArray();
        String username = args[4];
        pathToKeyAndTrustStores = pathToKeyAndTrustStores + username + "/";
        //String pathToKeystore = args[4];
        //String pathToTruststore = args[5];


        String host = null;
        int port = -1;
        for (int i = 0; i < args.length; i++) {
            System.out.println("args[" + i + "] = " + args[i]);
        }
        if (args.length < 4) {
            System.out.println("USAGE: java client host port");
            System.exit(-1);
        }
        try { /* get input parameters */
            host = args[0];
            port = Integer.parseInt(args[1]);
        } catch (IllegalArgumentException e) {
            System.out.println("USAGE: java client host port");
            System.exit(-1);
        }

        try { /* set up a key manager for client authentication */
            SSLSocketFactory factory = null;
            try {
                //char[] password = "password".toCharArray();
                KeyStore ks = KeyStore.getInstance("JKS");
                KeyStore ts = KeyStore.getInstance("JKS");

                KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
                TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
                SSLContext ctx = SSLContext.getInstance("TLSv1.2");
                ks.load(new FileInputStream(pathToKeyAndTrustStores + "clientkeystore"), clientKeyStorePasswd);  // keystore password (storepass)
				ts.load(new FileInputStream(pathToKeyAndTrustStores + "clienttruststore"), clientTrustStorePasswd); // truststore password (storepass);
                kmf.init(ks, clientKeyStorePasswd); // user password (keypass)
    

				tmf.init(ts); // keystore can be used as truststore here
                ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
    



                factory = ctx.getSocketFactory();
            } catch (Exception e) {
                throw new IOException(e.getMessage());
            }
            SSLSocket socket = (SSLSocket)factory.createSocket(host, port);
            System.out.println("\nsocket before handshake:\n" + socket + "\n");

            /*
             * send http request
             *
             * See SSLSocketClient.java for more information about why
             * there is a forced handshake here when using PrintWriters.
             */
            socket.startHandshake();
            SSLSession session = socket.getSession();
            X509Certificate cert = (X509Certificate)session.getPeerCertificates()[0];
            String subject = cert.getSubjectDN().getName();
            System.out.println("certificate name (subject DN field) on certificate received from server:\n" + subject + "\n");
            System.out.println("Issuer is" + cert.getIssuerDN().getName());
            System.out.println("Serial number is " + cert.getSerialNumber());
            System.out.println("socket after handshake:\n" + socket + "\n");
            System.out.println("secure connection established\n\n");

            UserInterface.runUserInterface(socket);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
