package project2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

import com.google.gson.Gson;

import protocol.Request;
import protocol.RequestOp;


public class UserInterface {

    public static void runUserInterface(Socket socket) throws IOException {
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));     
    
            login(out, in);

            var gson = new Gson();

			while(true) {
                printMenu();
                var request = createRequest();
                if (request == null) {
                    break;
                }
                System.out.println();
                out.println(request.toString());
                out.flush();
                var responseJson = in.readLine();
                String[] responseLines = gson.fromJson(responseJson, String[].class);
                for (String line : responseLines) {
                    System.out.println(line);
                }
            }
            in.close();
			out.close();
            socket.close();
    }

    private static void login(PrintWriter out, BufferedReader in) throws IOException {
        boolean authorized = false;
        do {
            System.out.println("Enter UserID: ");
            var userId = System.console().readLine();
            System.out.println("Enter password:");
            var password = System.console().readPassword();
            out.println(userId);
            out.println(password); // TODO hash?
            var answer = in.readLine();
            System.out.println(answer);
            authorized = answer.equals("success");
        } while (!authorized);
    }

    private static void printMenu() {
        System.out.println();
        System.out.println("Hello, what do you wanna do?"); 
        System.out.println("1. Read a record of patient");
        System.out.println("2. Add a new entry to patient's medical record");
        System.out.println("3. Create a medical record");
        System.out.println("4. Delete a medical record");
        System.out.println("5. List medical records for a patient");
        System.out.println("6. Exit");
    }

    private static Request createRequest() {
        int choice = readInt() - 1;
        var reqOps = RequestOp.values();

        if (choice == reqOps.length)
            return null;
        if (choice < 0 || choice > reqOps.length) {
            System.out.println("Please choose according to the menu");
            return createRequest();
        }

        Request req = null;
        switch (reqOps[choice]) {
            case READ_RECORD:
                req = readRecord();
                break;
            case ADD_ENTRY:
                req = addEntry();
                break;
            case CREATE_RECORD:
                req = createRecord();
                break;
            case DELETE_RECORD:
                req = deleteRecord();
                break;
            case LIST_RECORDS:
                req = listRecords();
                break;
            default:
                break;
        }
        return req;
    }

    private static int readInt() {
        int num = 0;
        boolean success = false;
        while (!success) {
            var text = System.console().readLine();
            try {
                num = Integer.parseInt(text);
                success = true;
            } catch (NumberFormatException e) {
                System.out.println("Incorrect number format. Try again:");
            }
        }
        return num;
    }
    
    private static Request readRecord() {
        System.out.println("Which record does it concern? Enter ID: ");
        var recordId = System.console().readLine();
        return Request.readRecord(recordId);
    }

    private static Request listRecords() {
        System.out.println("Which patient does it concern? Enter ID: ");
        var patientId = System.console().readLine();
        return Request.listRecords(patientId);
    }

    private static Request addEntry() {
        System.out.println("Which record does it concern? Enter ID: ");
        var recordId = System.console().readLine();
        System.out.println("What text do you want to add? Enter text: ");
        var text = System.console().readLine();
        return Request.addEntry(recordId, text);
    }

    private static Request createRecord() {
        System.out.println("Which patient does it concern? Enter ID: ");
        var patientId = System.console().readLine();
        System.out.println("What is the new record ID? Enter ID: ");
        var recordId = System.console().readLine();
        System.out.println("Which nurse should be associated? Enter ID: ");
        var nurseId = System.console().readLine();
        return Request.createRecord(recordId, patientId, nurseId);
    }

    private static Request deleteRecord() {
        System.out.println("Which record does it concern? Enter ID: ");
        var recordId = System.console().readLine();
        return Request.deleteRecord(recordId);
    }
}
