package protocol;

import com.google.gson.Gson;

public class Request {

    public RequestOp requestOp;
    public String recordId;
    public String patientId;
    public String text;
    
    private Request(RequestOp reqOp, String recordId, String patientId, String text) {
        this.requestOp = reqOp;
        this.recordId = recordId;
        this.patientId = patientId;
        this.text = text;
    }

    public static Request readRecord(String recordId) {
        return new Request(RequestOp.READ_RECORD, recordId, null, null);
    }

    public static Request addEntry(String recordId, String text) {
        return new Request(RequestOp.ADD_ENTRY, recordId, null, text);
    }

    public static Request createRecord(String recordId, String patientId, String nurseId) {
        return new Request(RequestOp.CREATE_RECORD, recordId, patientId, nurseId);
    }
    
    public static Request deleteRecord(String recordId) {
        return new Request(RequestOp.DELETE_RECORD, recordId, null, null);
    }
    
    public static Request listRecords(String patientId) {
        return new Request(RequestOp.LIST_RECORDS, null, patientId, null);
    }

    @Override
    public String toString() {
        var gson = new Gson();
        return gson.toJson(this);
    }
}