package protocol;

public class Request {

    public RequestOp requestOp;
    public String recordId;
    public String patientId;
    public String text;
}