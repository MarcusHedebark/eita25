package protocol;

import com.google.gson.annotations.SerializedName;

public enum RequestOp {
    @SerializedName("0")
    READ_RECORD,
    @SerializedName("1")
    ADD_ENTRY,
    @SerializedName("2")
    CREATE_RECORD,
    @SerializedName("3")
    DELETE_RECORD,
    @SerializedName("4")
    LIST_RECORDS;
}
