package project2;

import java.io.*;
import java.net.*;
import java.nio.file.Path;
import java.security.KeyStore;
import javax.net.*;
import javax.net.ssl.*;

import com.google.gson.Gson;

import dataaccess.AccessController;
import dataaccess.AccessDeniedException;
import dataaccess.AccessLog.AccessLogEntry;
import dataaccess.Database;
import dataaccess.LookupFailedException;
import protocol.Request;

import java.security.cert.X509Certificate;
import java.util.concurrent.locks.ReentrantLock;

public class ServerStarter implements Runnable {
    private ServerSocket serverSocket = null;
    private static Database db;
    private static ReentrantLock mutex = new ReentrantLock();
    private static int numConnectedClients = 0;
    private static String pathToKeyAndTrustStores = "../cert_project_2/ServerStuff/";
    private static KeyStore ts = null;

    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_RESET = "\u001B[0m";

    public ServerStarter(ServerSocket ss) throws IOException {
        serverSocket = ss;
        if (db == null) {
            var p1 = Path.of("data/subjects.json");
            var p2 = Path.of("data/records.json");
            var p3 = Path.of("data/accesslog.json");
            db = new Database(p1, p2, p3);
        }
        newListener();
    }

    private void goToMenu(PrintWriter out, BufferedReader in, SSLSocket socket) throws IOException {

        var user = authorizeUser(out, in);
        if (user == null)
            return;
        var userAc = new AccessController(user, db);
        var gson = new Gson();
        String requestJson = null;

        while ((requestJson = in.readLine()) != null) {
            var request = gson.fromJson(requestJson, Request.class);
            var accessLogEntry = new AccessLogEntry(user, request);
            try {
                switch (request.requestOp) {
                    case READ_RECORD:
                        var recordLines = userAc.readRecord(request.recordId);
                        out.println(gson.toJson(recordLines));
                        break;
                    case ADD_ENTRY:
                        userAc.addEntry(request.recordId, request.text);
                        out.println(toJsonArray("entry added"));
                        break;
                    case CREATE_RECORD:
                        userAc.createRecord(request.patientId, request.recordId, request.text);
                        out.println(toJsonArray("record created"));
                        break;
                    case DELETE_RECORD:
                        userAc.deleteRecord(request.recordId);
                        out.println(toJsonArray("record deleted"));
                        break;
                    case LIST_RECORDS:
                        var recordIds = userAc.listRecords(request.patientId);
                        out.println(gson.toJson(recordIds));
                        break;
                    default:
                        out.println("This shouldn't be happening :(");
                        break;
                }
            } catch (LookupFailedException e) {
                out.println(toJsonArray(e.getMessage()));
                accessLogEntry.setResult(e.getMessage());
            } catch (AccessDeniedException e) {
                out.println(toJsonArray(e.getMessage()));
                accessLogEntry.setResult(e.getMessage());
            }
            db.addAccessLogEntry(accessLogEntry);
        }
        db.saveState();
        in.close();
        out.close();
        socket.close();
        System.out.println("client disconnected");  
    }

    private String toJsonArray(String text) {
        String[] arr = {text};
        var gson = new Gson();
        return gson.toJson(arr);
    }

    private String authorizeUser(PrintWriter out, BufferedReader in) throws IOException {
        String userId = "";
        String password = "";
        boolean authorized = false;

        while(!authorized) {
            userId = in.readLine();
            if (userId == null)
                return null;
            password = in.readLine();
            try {
                var user = db.subject(userId);
                authorized = user.password.equals(password);
            } catch (LookupFailedException e) {}
            out.println(authorized ? "success" : "login failed");
        }
        return userId;
    }


 
  

    public void run() {
        try {
            SSLSocket socket=(SSLSocket)serverSocket.accept();
            newListener();
            SSLSession session = socket.getSession();
            var cert = (X509Certificate)session.getPeerCertificates()[0];
            String subject = cert.getSubjectDN().getName();
    	    numConnectedClients++;
            System.out.println("client connected");
            System.out.println("client name (cert subject DN field): " + subject);
            System.out.println("Issuer is" + cert.getIssuerDN().getName());
            System.out.println("Serial number is " + cert.getSerialNumber());
            System.out.println(numConnectedClients + " concurrent connection(s)\n");
            System.out.println(session.getCipherSuite());
            // Verify client certificate on server
 
            try {
                var ca = (X509Certificate) ts.getCertificate("hospital_ca");
                cert.verify(ca.getPublicKey());
            } catch (Exception e) {
                System.out.println("Verification failed.");
                System.out.println(e.getMessage());
                return;
            }
   
            System.out.println(ANSI_GREEN+"Verified certificate."+ANSI_RESET);
            PrintWriter out = null;
            BufferedReader in = null;
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            goToMenu(out, in, socket);
            //When process gets out of goToMenu the client has activiliy quit
            numConnectedClients--;

            
            /*while ((clientMsg = in.readLine()) != null) {
			    String rev = new StringBuilder(clientMsg).reverse().toString();
                System.out.println("received '" + clientMsg + "' from client");
                System.out.print("sending '" + rev + "' to client...");
				out.println(rev);
				out.flush();
                System.out.println("done\n");
			}*/
			
		} catch (IOException e) {
            System.out.println("Client died: " + e.getMessage());
            e.printStackTrace();
            return;
        }
    }

    private void newListener() { (new Thread(this)).start(); } // calls run()

    public static void main(String args[]) {
        System.out.println("Working Directory = " + System.getProperty("user.dir"));
        char[] serverKeyStorePasswd = args[1].toCharArray();
        char[] serverTrustStorePasswd = args[2].toCharArray();
        pathToKeyAndTrustStores = pathToKeyAndTrustStores + args[0] + "/";
        System.out.println("\nServer Started\n");
        int port = -1;
        if (args.length >= 3) {
            port = Integer.parseInt(args[0]);

        }
        String type = "TLSv1.2";
        try {
            ServerSocketFactory ssf = getServerSocketFactory(type,serverKeyStorePasswd,serverTrustStorePasswd);
            ServerSocket ss = ssf.createServerSocket(port);
            ((SSLServerSocket)ss).setNeedClientAuth(true); // enables client authentication
            new ServerStarter(ss);
        } catch (IOException e) {
            System.out.println("Unable to start Server: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private static ServerSocketFactory getServerSocketFactory(String type, char[] serverKeyStorePasswd,
            char[] serverTrustStorePasswd) {
        if (type.equals("TLSv1.2")) {
            SSLServerSocketFactory ssf = null;
            try { // set up key manager to perform server authentication
                SSLContext ctx = SSLContext.getInstance("TLSv1.2");
                KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
                TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
                KeyStore ks = KeyStore.getInstance("JKS");
				ts = KeyStore.getInstance("JKS");

                ks.load(new FileInputStream(pathToKeyAndTrustStores + "serverkeystore"), serverKeyStorePasswd);  // keystore password (storepass)
                ts.load(new FileInputStream(pathToKeyAndTrustStores + "servertruststore"), serverTrustStorePasswd); // truststore password (storepass)
                kmf.init(ks, serverKeyStorePasswd); // certificate password (keypass)
                tmf.init(ts);  // possible to use keystore as truststore here
                ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
                ssf = ctx.getServerSocketFactory();
                return ssf;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return ServerSocketFactory.getDefault();
        }
        return null;
    }
}
