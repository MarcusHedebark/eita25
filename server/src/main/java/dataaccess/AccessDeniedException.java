package dataaccess;

public class AccessDeniedException extends RuntimeException {
    final public String resource;

    public AccessDeniedException(String resource) {
        this.resource = resource;
    }
    
    @Override
    public String getMessage() {
        return "Access to " + resource + " denied.";
    }
}
