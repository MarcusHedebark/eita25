package dataaccess;

import com.google.gson.annotations.SerializedName;

public enum Roles {
    @SerializedName("0")
    GOVERNMENT,
    @SerializedName("1")
    PATIENT,
    @SerializedName("2")
    NURSE,
    @SerializedName("3")
    DOCTOR;
}
