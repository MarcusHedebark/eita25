package dataaccess;

import java.time.LocalDateTime;
import java.util.ArrayList;

import protocol.Request;

public class AccessLog {

    public ArrayList<AccessLogEntry> logs;

    public static class AccessLogEntry {
        public String date;
        public String accesserId;
        public Request request;
        public String result;

        public AccessLogEntry(String accesser, Request request) {
            this.accesserId = accesser;
            this.request = request;
            this.date = LocalDateTime.now().toString();
            result = "success";
        }

        public void setResult(String result) {
            this.result = result;
        }
    }
}
