package dataaccess;

import java.util.ArrayList;

public class Subjects {
    
    public ArrayList<Subject> subjects;

    public class Subject {
        public String division;
        public String subjectId;
        public Roles role;
        public String password;
        public ArrayList<String> doctors;
    }

    public String toString() {
        var out = new StringBuilder();
        for (var o : subjects) {
            out.append("subjectId: " + o.subjectId + ", division: " + o.division + "\n");
        }
        return out.toString();
    }
}
