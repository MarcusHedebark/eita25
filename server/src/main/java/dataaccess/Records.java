package dataaccess;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Records {

    public ArrayList<Record> records;

    class Record {
        public String recordId;
        public String patientId;
        public List<String> associatedIds;
        public List<Entry> entries;

        public Record(String patientId, String recordId, String... associatedIds) {
            this.recordId = recordId;
            this.patientId = patientId;
            this.associatedIds = Arrays.asList(associatedIds);
            this.entries = new ArrayList<Entry>();
        }

        class Entry {
            public String date;
            public String text;

            public Entry(String text) {
                this.date = LocalDateTime.now().toString();
                this.text = text;
            }

        }

        public String[] toStrings() {
            var list = new ArrayList<String>();
            for (Entry entry : entries) {
                list.add("Date: " + entry.date);
                for (String line : entry.text.split("\n")) {
                    list.add(line);
                }
            }
            return list.toArray(new String[0]);
        }
    }
}
