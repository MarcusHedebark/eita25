package dataaccess;

public class LookupFailedException extends RuntimeException {

    final public String missing;

    public LookupFailedException(String missing) {
        this.missing = missing;
    }

    @Override
    public String getMessage() {
        return "Could not find: " + missing + ".";
    }
}
