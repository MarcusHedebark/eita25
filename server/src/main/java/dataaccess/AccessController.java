package dataaccess;

import dataaccess.Records.Record;
import dataaccess.Subjects.Subject;

public class AccessController {
    Database db;
    Subject accesser;
    boolean isNurseOrDoctor;
    boolean isGovernment;

    public AccessController(String accesserId, Database db) {
        this.accesser = db.subject(accesserId);
        this.db = db;
        this.isNurseOrDoctor = accesser.role == Roles.NURSE || accesser.role == Roles.DOCTOR;
        this.isGovernment = accesser.role == Roles.GOVERNMENT;
    }

    public String[] readRecord(String recordId) {
        var record = db.record(recordId);
        var patientId = record.patientId;
        var patient = db.subject(patientId);
        var isTreater = record.associatedIds.contains(accesser.subjectId);
        var isPatient = patientId.equals(accesser.subjectId);
        var isSameDivision = patient.division.equals(accesser.division);
        var canAccess = isTreater || isPatient || isGovernment || (isNurseOrDoctor && isSameDivision);

        if (canAccess)
            return record.toStrings();

        throw new AccessDeniedException("record " + recordId);
    }

    public void addEntry(String recordId, String text) {
        var record = db.record(recordId);
        var isAssociated = record.associatedIds.contains(accesser.subjectId);
        var canAccess = isNurseOrDoctor && isAssociated;

        if (canAccess)
            db.addEntry(recordId, text);
        else
            throw new AccessDeniedException("record " + recordId);
    }

    public void deleteRecord(String recordId) {
        var canDelete = accesser.role == Roles.GOVERNMENT;

        if (canDelete)
            db.deleteRecord(recordId);
        else
            throw new AccessDeniedException("record " + recordId);
    }

    public void deleteAllRecordsFor(String patientId) {
        for (Record record : db.allRecordsFor(patientId)) {
            deleteRecord(record.recordId);
        }
    }

	public void createRecord(String patientId, String recordId, String associatedId) {
        var accesserId = accesser.subjectId;
        var patient = db.subject(patientId);
        var isDoctor = accesser.role == Roles.DOCTOR;
        var isDoctorOfSubject = patient.doctors.contains(accesserId);
        var nurse = db.subject(associatedId);
        var isNurseSameDivision = accesser.division.equals(nurse.division);
        var canCreate = isDoctor && isDoctorOfSubject && isNurseSameDivision;

        if (canCreate)
            db.createRecord(patientId, recordId, accesserId, associatedId);
        else
            throw new AccessDeniedException("patient " + patientId);
    }

    public String[] listRecords(String patientId) {
        var patient = db.subject(patientId);
        var isDoctor = accesser.role == Roles.DOCTOR;
        var isDoctorOfSubject = patient.doctors.contains(accesser.subjectId);
        var canAccess = isDoctor && isDoctorOfSubject;

        if (canAccess)
            return db.listRecords(patientId);

        throw new AccessDeniedException("patient " + patientId);
    }
}
