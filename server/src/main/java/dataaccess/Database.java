package dataaccess;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Optional;

import com.google.gson.Gson;

import dataaccess.AccessLog.AccessLogEntry;
import dataaccess.Records.Record;
import dataaccess.Subjects.Subject;

public class Database {

    public AccessLog accessLog;
    public Subjects subjects;
    public Records records;
    private Path subjectsPath;
    private Path recordsPath;
    private Path accessLogPath;


    public Database(Path subjectsPath, Path recordsPath, Path accessLogPath) throws IOException {
        this.subjectsPath = subjectsPath;
        this.recordsPath = recordsPath;
        this.accessLogPath = accessLogPath;

        var gson = new Gson();

        var jsonSubjects = Files.readString(subjectsPath);
        subjects = gson.fromJson(jsonSubjects, Subjects.class);

        var jsonRecords = Files.readString(recordsPath);
        records = gson.fromJson(jsonRecords, Records.class);

        var jsonAccessLog = Files.readString(accessLogPath);
        accessLog = gson.fromJson(jsonAccessLog, AccessLog.class);
    }

    public void saveState() throws IOException {
        var gson = new Gson();
        var jsonSubjects = gson.toJson(subjects);
        var jsonRecords = gson.toJson(records);
        var jsonAccessLog = gson.toJson(accessLog);

        Files.delete(subjectsPath);
        Files.delete(recordsPath);
        Files.delete(accessLogPath);

        Files.writeString(subjectsPath, jsonSubjects, StandardOpenOption.CREATE_NEW);
        Files.writeString(recordsPath, jsonRecords, StandardOpenOption.CREATE_NEW);
        Files.writeString(accessLogPath, jsonAccessLog, StandardOpenOption.CREATE_NEW);
    }

    public boolean correctIdAndPassword(String subjectId, String password) {
        var subject = subject(subjectId);
        return subject.password.equals(password);
    }

    public void addEntry(String recordId, String text) {
        var record = record(recordId);
        record.entries.add(record.new Entry(text));
    }

    public void addAccessLogEntry(AccessLogEntry entry) {
        accessLog.logs.add(entry);
    }

    public ArrayList<Record> allRecordsFor(String patientId) {
        var list = new ArrayList<Record>();
        for (Record record : records.records) {
            if (record.patientId.equals(patientId)) {
                list.add(record);
            }
        }
        return list;
    }

    public String[] listRecords(String patientId) {
        var list = new ArrayList<String>();
        for (Record record : records.records) {
            if (record.patientId.equals(patientId))
                list.add(record.recordId);
        }
        return list.toArray(new String[0]);
    }

    public void deleteRecord(String recordId) {
        var record = record(recordId);
        records.records.remove(record);
    }

    public void createRecord(String patientId, String recordId, String... associatedIds) {
        var record = records.new Record(patientId, recordId, associatedIds);
        records.records.add(record);
    }

    public Record record(String recordId) {
        return linearSearch(records.records, (x) -> x.recordId, recordId)
                .orElseThrow(() -> new LookupFailedException("record " + recordId));
    }

    public Subject subject(String subjectId) {
        return linearSearch(subjects.subjects, (x) -> x.subjectId, subjectId)
                .orElseThrow(() -> new LookupFailedException("accesser " + subjectId));
    }

    public <T, E> Optional<T> linearSearch(ArrayList<T> list, LookUpInterface<T, E> lookup, E elem) {
        for (T obj : list) {
            if (lookup.lookup(obj).equals(elem)) {
                return Optional.of(obj);
            }
        }
        return Optional.empty();
    }

    @FunctionalInterface
    interface LookUpInterface<T, E> {
        E lookup(T obj);
    }
}
