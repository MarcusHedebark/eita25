package dataaccess;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.io.IOException;
import java.nio.file.Path;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DataAccessTest {

    Database db;
    AccessController govAc;
    AccessController patAc;
    AccessController nurAc;
    AccessController docAc;

    @BeforeEach
    void createDatabase() throws IOException {
        System.out.println("Working Directory = " + System.getProperty("user.dir"));
        var p1 = Path.of("data/testsubjects.json");
        var p2 = Path.of("data/testrecords.json");
        var p3 = Path.of("data/testaccesslog.json");
        db = new Database(p1, p2, p3);
        govAc = new AccessController("gov", db);
        patAc = new AccessController("pat", db);
        nurAc = new AccessController("nur", db);
        docAc = new AccessController("doc", db);
    }

    @Test
    void testDatabase() {
        var objectsString = "subjectId: gov, division: hej\nsubjectId: pat, division: 123\nsubjectId: nur, division: 123\nsubjectId: doc, division: 123\n";
        assertEquals(objectsString, db.subjects.toString());
    }

    @Test
    void testReadRecord() {
        String[] expected = {"Date: 2010-02-21","have a nice day! :)","Date: 2010-02-20","hello world","and hello to you too!"};
        assertArrayEquals(expected, govAc.readRecord("abc"));
        assertArrayEquals(expected, patAc.readRecord("abc"));
        assertArrayEquals(expected, nurAc.readRecord("abc"));
        assertArrayEquals(expected, docAc.readRecord("abc"));
        assertThrows(AccessDeniedException.class, ()->nurAc.readRecord("xyz"));
        assertThrows(AccessDeniedException.class, ()->patAc.readRecord("xyz"));
        assertThrows(LookupFailedException.class, ()->patAc.readRecord("aoeu"));
    }

    @Test
    void testAddEntry() {
        var entryText = "ujujujujujuj";
        assertThrows(AccessDeniedException.class, ()->nurAc.addEntry("xyz", entryText));
        docAc.addEntry("xyz", entryText);
        var entry = db.record("xyz").entries.get(0);
        assertEquals(entryText, entry.text);
    }

    @Test
    void testDeleteRecord() {
        assertThrows(AccessDeniedException.class, ()->docAc.deleteAllRecordsFor("pat"));
        govAc.deleteAllRecordsFor("pat");       
        assertThrows(LookupFailedException.class, ()->db.record("pat"));
    }
    
    @Test
    void testCreateRecord() {
        assertThrows(AccessDeniedException.class, ()->nurAc.deleteAllRecordsFor("pat"));
        docAc.createRecord("pat", "new", "nur");
        var record = db.record("new");
        assertTrue(record.associatedIds.contains("doc"));
        assertTrue(record.associatedIds.contains("nur"));
        assertTrue(record.patientId.equals("pat"));
    }

    @Test
    void testAuthentication() {
        assertTrue(db.correctIdAndPassword("pat", "password123"));
        assertFalse(db.correctIdAndPassword("pat", "1234567890"));
    } 
}
