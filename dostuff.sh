openssl req -newkey rsa:4096 -x509 -sha256 -nodes -out cert.crt -keyout cert.key
keytool -import -file cert.crt -alias cert -keystore clienttruststore
keytool -genkey -keyalg RSA -alias Bertil -keystore clientkeystore 
keytool -keystore clientkeystore -certreq -alias Bertil -keyalg rsa -file roberta.csr
openssl x509 -req -CA cert.crt -CAkey cert.key -in roberta.csr -out roberta.crt -CAcreateserial
keytool -import -keystore clientkeystore -file cert.crt -alias cert
keytool -import -keystore clientkeystore -file roberta.crt -alias Bertil

keytool -genkey -keyalg RSA -alias ServerBengt -keystore serverkeystore
keytool -keystore serverkeystore -certreq -alias ServerBengt -keyalg rsa -file myserver.csr
openssl x509 -req -CA cert.crt -CAkey cert.key -in myserver.csr -out myserver.crt -CAcreateserial
keytool -import -keystore serverkeystore -file cert.crt -alias cert
keytool -import -keystore serverkeystore -file myserver.crt -alias ServerBengt
keytool -import -file cert.crt -alias cert -keystore servertruststore